/*

Graded ACtivity: s19 a1

Create a new set of pokemon for pokemon battle.
Solve the health of the pokemon when tackle is invoked, current value of health should decrease.

if health is now below 10, invoke faint function

*/

function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl ;
	this.tackle =  function(target){
		console.log(target)	//contains charizard obj
		if(target.health < 10) {
			return target.faint();
			// console.log(target.faint());
		};
		console.log(`${target.name}'s health is ${target.health}`)

		console.log(`${this.name} tackled ${target.name}`);
		

		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
		

		target.health -= this.attack;
		if(target.health < 10) {
			return target.faint();
			// console.log(target.faint());
		};


	};
	this.faint = function(){
		console.log(`${this.name} fainted`);
	}
}

let meowth = new Pokemon("Meowth", 6, 10)
let zapdos = new Pokemon("Zapdos", 12, 10)

console.log(meowth.tackle(zapdos))
console.log(meowth.tackle(zapdos))
console.log(meowth.tackle(zapdos))
console.log(meowth.tackle(zapdos))
console.log(meowth.tackle(zapdos))
